<?php

require_once ROOT . '/configs/db-config.php';

/**
 * Class DBManager
 *
 * класс оберка для работы с PDO
 */
class DBManager
{
    private static $inst = null;
    private $dbhost;
    private $dblogin;
    private $dbpass;
    private $dbname;
    private $link;
    private $pdo;
    private $orm;

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    private function __construct()
    {
        $hosts = config();
        $this->dbhost = $hosts['host'];
        $this->dblogin = $hosts['dblogin'];
        $this->dbpass = $hosts['dbpass'];
        $this->dbname = $hosts['dbname'];
        $this->link = null;
        $this->pdo = null;
        $this->orm = null;
    }

    public static function getInst()
    {
        if (self::$inst === null) {
            self::$inst = new self;
        }
        return self::$inst;
    }

    /**
     * @return Database|null
     */
    public function getOrm()
    {

        if ($this->orm == null) {

            $connection = new Opis\Database\Connection(
                'mysql:host=' . $this->dbhost . ';dbname=' . $this->dbname,
                $this->dblogin,
                $this->dbpass
            );
            $this->orm = new Opis\Database\Database($connection);
        }
        return $this->orm;
    }

    public function getPDO()
    {
        if ($this->pdo == null) {
            try {
                $this->pdo = new PDO('mysql:host=' . $this->dbhost . ';dbname=' . $this->dbname, $this->dblogin, $this->dbpass);
                $this->pdo->exec("SET NAMES UTF8");
            } catch (PDOException $e) {
                print "Error!: " . $e->getMessage() . "<br/>";
                die();
            }
        }
        return $this->pdo;
    }

    public function getLink()
    {
        if ($this->link == null) {
            $this->link = mysqli_connect($this->dbhost, $this->dblogin, $this->dbpass);
            mysqli_set_charset($this->link, 'utf8');
            mysqli_select_db($this->link, $this->dbname) or die(mysqli_error($this->link));
        }
        return $this->link;
    }
}