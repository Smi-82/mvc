<?php

/**
 * Class Helper
 * для дебага
 */
class Helper
{
    private static $instance;

    public static function getInst()
    {
        if(!self::$instance)
            self::$instance = new self();
        return self::$instance;
    }

    public function debug($arr, $fl = true)
    {
        echo '<pre>';
        $fl ? var_dump($arr) : print_r($arr);
        echo '</pre>';
    }

}