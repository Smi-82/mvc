<?php

include_once DB_MANAGER;

/**
 * Class Model
 */
class Model
{
    private $db;
    private $table;

    /**
     * @param $arr
     * @return array
     *
     * метод для создания массива данных подготовленного для работы с PDO
     * пример ':id'=>value
     */
    protected function getQueryArr($arr)
    {
        $tmp = array();
        foreach($arr as $key => $val)
            $tmp[':'.$key] = $val;
        return $tmp;
    }

    /**
     * @return mixed
     * обертка для получения id последней вставленной записи в БД
     */
    protected function getLastID()
    {
        return $this->getDB()->getPDO()->lastInsertId();
    }

    /**
     * @param $str
     * @return string
     * метод для получения строки вида id=:id
     */
    protected function getQueryParam($str)
    {
        return $str.'=:'.$str;
    }
    public function __construct()
    {
        $this->db = DBManager::getInstance();
    }

    /**
     * @return mixed
     *
     * оберка для получения подлючения к БД
     */
    public function getDB()
    {
        return $this->db;
    }

}