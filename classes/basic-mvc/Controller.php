<?php

/**
 * Class Controller
 * подключает файлы модели и представления
 *
 */
include HELPER;

class Controller extends Helper
{
    /**
     * @param string $name
     * @param $obj
     * @return mixed
     */
    private function getObj($name = null, $obj)
    {
        if (!is_null($name)) {
            $file = ROOT;
            $path = ROOT;
            $module = trim($_SERVER['REQUEST_URI'], '/');
            $module = explode('/', $module);
            $module_dir = array_shift($module);
            if ($module_dir == 'module') {
                $file .= DS . 'modules' . DS;

                $module_name = array_shift($module);
                if (empty($module_name) || !file_exists($file . $module_name)) {
                    $module_name = 'index';
                }
                $file .= $module_name;
                $path .= DS . 'modules' . DS . $module_name;
            }
            $file .= DS . mb_strtolower($obj) . 's' . DS . $name . '.php';
//            return $file;
            try {
                if (file_exists($file)) {
                    require_once $file;
                    $cl = $name;
                    return new $cl($path);
                } else
                    throw new Exception($name . ' does not exists');
            } catch (Exception $e) {
                echo $e->getMessage();
                die;
            }
        }
    }

    /**
     * @return View
     *
     * возвращает обьект представления
     */

    public function getView($name = '')
    {
        return $this->getObj($name, 'View');
    }

    /**
     * @return Model
     * возвращает обьект модели
     */
    public function getModel($name = '')
    {
        return $this->getObj($name, 'Model');
    }

}