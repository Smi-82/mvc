<div class="footer">
    <div class="<?= getBsCl(array('lg' => 8, 'md' => 6, 'sm' => 6, 'xs' => 6)); ?>">
        <div class="row">
            <div class="<?= getBsCl(4); ?>">
                <label for="user_name">Имя</label>
                <input type="text" id="user_name" class="form-control" data-valid="true">
                <label for="email">E-mail</label>
                <input type="text" id="email" class="form-control" data-valid="true">
            </div>
            <div class="<?= getBsCl(8); ?>">
                <label for="msg">Сообщение</label>
                <textarea name="" id="msg" class="form-control"></textarea>
            </div>
        </div>
    </div>
    <div class="<?= getBsCl(array('lg' => 2, 'md' => 3, 'sm' => 3, 'xs' => 3)); ?> file_container">
        <div>
            <button type="button" class="btn btn-info" id="getFile_btn">Выберите файл
        </div>
        </button>
        </span>
        <div style="margin-top: 10px">
            <input disabled type="text" id="for_photoloader" class="form-control">
        </div>
        <form enctype="multipart/form-data" action="/app/upload_file.php" method="POST" id="othdetphotoform" target="hiddenframe" style="display: none">
            <input name="file" type="file" id="photoloader"/>
            <input type="reset" id="reset_photoloader"/>
        </form>
        <iframe id="hiddenframe" name="hiddenframe" style="display: none;"></iframe>
    </div>
    <div class="<?= getBsCl(array('lg' => 2, 'md' => 3, 'sm' => 3, 'xs' => 3)); ?>">
        <div class="btn btn-primary" id="send">Отправить</div>
        <div class="btn btn-warning" id="preview_btn" style="margin-top: 10px">Предварительный<br> просмотр</div>
    </div>
</div>