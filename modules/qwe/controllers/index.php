<?php

namespace modules\qwe\controllers;
include_once CONTROLLER;
/**
 * Class PersonalAreaController
 * контроллер для обработки данных на странице index
 */
class Index extends \Controller
{
    /**
     * вызов вида
     */
    public function index()
    {
        $view = $this->getView('index');
        $view->run();
    }
}