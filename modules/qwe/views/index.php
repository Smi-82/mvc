<?php

include_once VIEW;

/**
 * Class IndexView
 *
 * вид для отображения шаблона index (по умолчанию)
 */
class Index extends View
{
    public function run()
    {
        $this->setStyles([
            'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css'
        ]);
        $this->setScripts([
            'https://code.jquery.com/jquery-3.4.1.min.js',
            'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js',
        ]);
        $this->showLayout();
//        return 'view';
    }
}