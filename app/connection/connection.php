<!--bootstrap-->
<link rel="stylesheet" href="app/css/bootstrap.min.css">
<link rel="stylesheet" href="app/css/bootstrap-theme.min.css">
<!--fonts-->
<link rel="stylesheet" href="app/css/font-awesome.min.css">
<!--myStyles-->
<link rel="stylesheet" href="app/css/custom.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script src="app/js/jquery-3.1.0.min.js"></script>

<script src="app/js/bootstrap.min.js"></script>
<script src="app/js/app.js"></script>
<?php $this->getScripts();?>
