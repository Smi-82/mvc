<?php

$arr = array('jpeg', 'jpg', 'png', 'gif');

$file_info = pathinfo($_FILES['file']['name']);

/**
 * проверка расширений файла
 */
if (!in_array($file_info['extension'], $arr))
    return 'error';

// заданные размеры
$n_w = 320;
$n_h = 240;

// фактические размеры
$cur_w = getimagesize($_FILES['file']['tmp_name'])[0];
$cur_h = getimagesize($_FILES['file']['tmp_name'])[1];
//print_r(getimagesize($_FILES['file']['tmp_name']));
//die;

// необходимость изменения изображения
if ($cur_w > $n_w || $cur_h > $n_h) {

    $image = new ChangeImage();
    $image->load($_FILES['file']['tmp_name']);

    if ($cur_w > $cur_h)
        $image->resizeToWidth($n_w);
    else
        $image->resizeToHeight($n_h);

    $image->save($_FILES['file']['tmp_name']);
}

// новое название файла
$new_name = md5($file_info['filename']) . '.' . $file_info['extension'];

// Загрузка файла
$uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/files/';

// создание директории в корне сайта при ее отсутствии
if (!file_exists($uploaddir))
    mkdir($uploaddir);

chmod($uploaddir, 0777);

$uploadfile = $uploaddir . basename($_FILES['file']['name']);

$file_info = pathinfo($uploadfile);

session_start();

if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {

    $new_name = $uploaddir . $new_name;
    rename($uploadfile, $new_name);
    chmod($new_name, 0777);
    $_SESSION['file_path'] = '/' . implode('/', array_slice(explode('/', $new_name), -2));
//    print_r('/'.implode('/',array_slice(explode('/',$new_name),-2)));
//    die;
    echo 'ok';
} else
    echo 'error';


/**
 * Class ChangeImage
 * класс обработки изображнеий до заданного размера
 */
class ChangeImage
{
    private $image;
    private $image_type;

    private function getWidth()
    {
        return imagesx($this->image);
    }

    private function getHeight()
    {
        return imagesy($this->image);
    }

    public function load($filename)
    {
        $image_info = getimagesize($filename);
        $this->image_type = $image_info[2];
        switch ($this->image_type) {
            case IMAGETYPE_JPEG:
                $this->image = imagecreatefromjpeg($filename);
                break;
            case IMAGETYPE_GIF:
                $this->image = imagecreatefromgif($filename);
                break;
            case IMAGETYPE_PNG:
                $this->image = imagecreatefrompng($filename);
                break;
        }
    }

    public function save($filename, $image_type = IMAGETYPE_JPEG, $compression = 75, $permissions = null)
    {
        switch ($image_type) {
            case IMAGETYPE_JPEG:
                imagejpeg($this->image, $filename, $compression);
                break;
            case IMAGETYPE_GIF:
                imagegif($this->image, $filename);
                break;
            case IMAGETYPE_PNG:
                imagepng($this->image, $filename);
                break;
        }

        if ($permissions != null) {
            chmod($filename, $permissions);
        }
    }

    public function output($image_type = IMAGETYPE_JPEG)
    {
        switch ($image_type) {
            case IMAGETYPE_JPEG:
                imagejpeg($this->image);
                break;
            case IMAGETYPE_GIF:
                imagegif($this->image);
                break;
            case IMAGETYPE_PNG:
                imagepng($this->image);
                break;
        }
    }

    public function resizeToHeight($height)
    {
        $ratio = $height / $this->getHeight();
        $width = $this->getWidth() * $ratio;
        $this->resize($width, $height);
    }

    public function resizeToWidth($width)
    {
        $ratio = $width / $this->getWidth();
        $height = $this->getheight() * $ratio;
        $this->resize($width, $height);
    }

    public function scale($scale)
    {
        $width = $this->getWidth() * $scale / 100;
        $height = $this->getheight() * $scale / 100;
        $this->resize($width, $height);
    }

    public function resize($width, $height)
    {
        $new_image = imagecreatetruecolor($width, $height);
        imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
        $this->image = $new_image;
    }
}