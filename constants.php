<?php
/**
 * задание констант для приложения
 */
session_start();

define('ROOT', dirname(__FILE__));
define('CONTROLLER', ROOT . '/classes/basic-mvc/Controller.php');
define('MODEL', ROOT . '/classes/basic-mvc/Model.php');
define('VIEW', ROOT . '/classes/basic-mvc/View.php');
define('FUNCTIONS', ROOT . '/app/functions.php');
define('HELPER', ROOT . '/classes/basic-mvc/Helper.php');
define('DB_MANAGER', ROOT . '/classes/db/db-manager.php');
define('CONNECTIONS', ROOT . '/app/connection/');
define('SCRIPTS', 'app/js/');
define('STYLES', 'app/css/');
define('DS', DIRECTORY_SEPARATOR);
define('BASE_PATH', __DIR__ . DS);