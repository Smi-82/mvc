<?php

include_once 'constants.php';
include_once FUNCTIONS;
require BASE_PATH . 'vendor/autoload.php';

$app = System\App::instance();

$app->request = System\Request::instance();
$app->route = System\Route::instance($app->request);

$route = $app->route;
$route->group('/module', function(){
    $this->any('/', 'modules\index\controllers\Index@index');
    $this->any('/qwe', 'modules\qwe\controllers\Index@index');
});
$route->any('/', 'controllers\Index@index');

$route->end();